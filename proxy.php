<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: X-Requested-With');

function fetchRoute($id)
{
    $filename = 'cache/'.$id.'.json';

    if ($json = @file_get_contents($filename)) {
        return json_decode($json);
    }

    $curl = curl_init('http://www.afstandmeten.nl/loadRoute.php');

    $data = array(
        'id' => $id,
    );

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);

    file_put_contents($filename, $response);

    return json_decode($response);
}

$json = fetchRoute($_GET['id']);

$route = $json->routes[0]->route;

$result = array(
    'id' => (int) $route->id,
    'distance' => (float) $route->distance,
    'center' => array(
        'lat' => (float) $route->center_lat,
        'lng' => (float) $route->center_lng,
    ),
    'points' => array_map(function ($point) {
        return array(
            'lat' => (float) $point->point->lat,
            'lng' => (float) $point->point->lng,
        );
    }, $route->points),
);

header('Content-Type: application/json');

echo json_encode($result);
