<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: OPTIONS, GET, POST');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, X-Requested-With');

$json = json_decode(file_get_contents('routes.json'));

$id = $_GET['id'];
$property = $_GET['property'];
$value = $_GET['value'];

if (isset($json->routes->{$id}->{$property})) {
    switch ($property) {
        default:
            // don't set
            break;
        case 'enabled':
        case 'open':
            $json->routes->{$id}->{$property} = (bool) $value;
            break;
        case 'slowSpeed':
        case 'fastSpeed':
            $json->routes->{$id}->{$property} = (float) $value;
            break;
        case 'startTime':
            $date = new DateTime($value);
            $json->routes->{$id}->{$property} = $date->format('Y-m-d H:i');
            break;
    }
}

header('Content-Type: application/json');

file_put_contents('routes.json', json_encode($json));

echo json_encode($json);
